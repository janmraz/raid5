#ifndef __PROGTEST__
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <cstdint>
#include <cassert>
#include <iostream>

using namespace std;

const int SECTOR_SIZE                                      =             512;
const int MAX_RAID_DEVICES                                 =              16;
const int MAX_DEVICE_SECTORS                               = 1024 * 1024 * 2;
const int MIN_DEVICE_SECTORS                               =    1 * 1024 * 2;

const int RAID_STOPPED                                     = 0;
const int RAID_OK                                          = 1;
const int RAID_DEGRADED                                    = 2;
const int RAID_FAILED                                      = 3;

// deprecated
const int RAID_DEVICE_OK                                   = 0;
const int RAID_DEVICE_FAILED                               = 1;

struct TBlkDev
{
    int              m_Devices;
    int              m_Sectors;
    int           (* m_Read )  ( int, int, void *, int );
    int           (* m_Write ) ( int, int, const void *, int );
};
#endif /* __PROGTEST__ */
namespace CRaidNamespace {

    class CRaidVolume {
    public:
        static bool Create(const TBlkDev &dev);

        int Start(const TBlkDev &dev);

        int Stop(void);

        int Resync(void);

        int Status(void) const;

        int Size(void) const;

        bool Read(int secNr,
                  void *data,
                  int secCnt);

        bool Write(int secNr,
                   const void *data,
                   int secCnt);

        void getRealPositionInRAID(int sectorNumber, int &column, int &lineNumber) const {
            // calculate line by dividing by devices (minus one)
            lineNumber = sectorNumber / (disksCount - 1);

            // get the beginning of line
            int sectorAtBeginningOfLine = lineNumber * (disksCount - 1);

            // get parity column in that line
            int parityColumn = lineNumber % disksCount;

            // get the position in line
            int linePosition = sectorNumber - sectorAtBeginningOfLine;

            // if parity column is before then we have move one step ahead
            if (parityColumn <= linePosition)
                linePosition++;

            // save column
            column = linePosition;
        }

        void initialize(const TBlkDev &dev) { // TODO make this as constructor
            // save the internal read and write
            _read = dev.m_Read;
            _write = dev.m_Write;
            // set the disks count and sectors count
            disksCount = MAX_RAID_DEVICES < dev.m_Devices ? MAX_RAID_DEVICES : dev.m_Devices;
            sectors = MAX_DEVICE_SECTORS < dev.m_Sectors ? MAX_DEVICE_SECTORS : dev.m_Sectors;

            // set initial values
            state = RAID_STOPPED;
            brokenDiskNumber = -1;
            version = 1;
        }

    protected:

        // wrapper around internal read function
        bool read(int diskNumber, int sectorNumber, void *data, int sizeInSectors);

        // wrapper around internal write function
        bool write(int diskNumber, int sectorNumber, const void *data, int sizeInSectors);

        // status of RAID - RAID_OK, RAID_DEGRADED, RAID_FAILED, RAID_STOPPED
        int state = RAID_STOPPED;

        // number of disks in RAID
        int disksCount;

        // which disk is broken ?
        int brokenDiskNumber;

        // deprecated
//        int deviceStates[MAX_RAID_DEVICES];

        // number of total sectors which are presented to users (can be used)
        int sectors;

        // version of disk RAID - used to recovery ensureness
        unsigned int version;

        // internal read function - which is given on startup
        int (*_read )(int, int, void *, int){};

        // internal write function - which is given on startup
        int (*_write )(int, int, const void *, int){};
    };

    static CRaidVolume raid;
}

using namespace CRaidNamespace;

bool CRaidVolume::read(int diskNumber, int sectorNumber, void *data, int sizeInSectors) {
    // you cannot read from broken disk
    if (raid.brokenDiskNumber == diskNumber) return false;

    // get internal read operation
    int readSections = _read(diskNumber, sectorNumber, data, sizeInSectors);

    // check if readed section is same as it supposed to be
    if (readSections != sizeInSectors) {

        // there is broken disk already so we lost our data
        // otherwise set broken disk index number and set raid to DEGRADED
        if (raid.brokenDiskNumber != -1) {
            raid.state = RAID_FAILED;
        } else {
            raid.brokenDiskNumber = diskNumber;
            raid.state = RAID_DEGRADED;
        }
        return false;
    };
    return true;
}

bool CRaidVolume::write(int diskNumber, int sectorNumber, const void *data, int sizeInSectors) {

    // you cannot write into broken disk
    if (raid.brokenDiskNumber == diskNumber) return false;

    // get internal write operation
    int writtenSections = _write(diskNumber, sectorNumber, data, sizeInSectors);

    // check if all sectors have been written correctly
    if (writtenSections != sizeInSectors) {

        // there is broken disk already so we lost our data
        // otherwise set broken disk index number and set raid to DEGRADED
        if (raid.brokenDiskNumber != -1) {
            raid.state = RAID_FAILED;
        } else {
            raid.brokenDiskNumber = diskNumber;
            raid.state = RAID_DEGRADED;
        }
        return false;
    };
    return true;
}

bool CRaidVolume::Create(const TBlkDev &dev) {
    // copy all the necessary things
    raid.initialize(dev);

    // copy version into its own variable
    char versionNumber[SECTOR_SIZE];
    memcpy(versionNumber, &raid.version, sizeof(unsigned int));

    // counter for all broken devices
    int countOfBrokenDevices = 0;

    // iterate over all disks
    for (int i = 0; i < raid.disksCount; i++) {

        // write version number to disk to the last sector
        if (!raid.write(i, raid.sectors - 1, versionNumber, 1)) {
            // if failed then increment the counter of broken devices
            countOfBrokenDevices++;

            // set failed disk index
            raid.brokenDiskNumber = i;
        }
    }

    return countOfBrokenDevices <= 1; // TODO check - is this correct ??
}

int CRaidVolume::Start(const TBlkDev &dev) {

    // copy all the necessary things
    raid.initialize(dev);

    // crete array for all raid devices with their versions - set them all to 0
    unsigned int versions[MAX_RAID_DEVICES];
    memset(versions, 0, sizeof(unsigned int) * MAX_RAID_DEVICES);

    // move container
    char newVersion[SECTOR_SIZE];

    // iterate over all devices and gather their versions
    for (int i = 0; i < raid.disksCount; i++)
    {
        // read version number and store into container variable
        if (!raid.read(i, raid.sectors - 1, newVersion, 1))
        {
            // if another disk is failed then our data ara lost forever -> return failed state
            if (raid.state == RAID_FAILED) return raid.state;

            // if it fails then the device is broken, set the broken index and continue
            raid.brokenDiskNumber = i;
            raid.state = RAID_DEGRADED;
            continue; // TODO maybe there could be problem with not filled version
        }

        // copy container into version variable
        unsigned int currentVersion;
        memcpy(&currentVersion, newVersion, sizeof(unsigned int));

        // save the version
        versions[i] = currentVersion;
    }

    // create variables for majority max count and majority index
    int majorityMaxCount = 0;
    int majorityIndex = -1;

    // iterate over disks
    for (int i = 0; i < raid.disksCount; i++)
    {
        // create counter for version
        int countOfCurrentVersion = 0;

        // iterate again over disks and get the count of certain version
        for(int j = 0; j < raid.disksCount; j++)
            if(versions[i] == versions[j])
                countOfCurrentVersion++;

        // if the count of current version is bigger then set the version and its index
        if(countOfCurrentVersion > majorityMaxCount)
        {
            majorityMaxCount = countOfCurrentVersion;
            majorityIndex = i;
        }
    }

    // set the currect version as majority index
    raid.version = versions[majorityIndex];

    // iterate over all devices
    for (int j = 0; j < raid.disksCount; j++) {
        // if it is not a majority version
        if (versions[j] != raid.version) {

            // if there is broken disk and this one is not the one broken then we have ultimately lost our data
            if (raid.brokenDiskNumber != -1 && raid.brokenDiskNumber != j) {
                raid.state = RAID_FAILED;
                return raid.state;
            }

            // set the broken index
            raid.brokenDiskNumber = j;
        }
    }


    // if there is no broken disk then return OK, otherwise there is one broken disk so return DEGRADED (more disks is handled inside function)
    if (raid.brokenDiskNumber == -1)
        raid.state = RAID_OK;
    else
        raid.state = RAID_DEGRADED;

    // return current state
    return raid.state;
}

int CRaidVolume::Stop() {
    // if the state is failed we can change state into STOPPED state
    // if the state is stopped already then ignore it
    // state OK and DEGRADED allow
    if (raid.state == RAID_FAILED || raid.state == RAID_STOPPED)
    {
        raid.state = RAID_STOPPED;
        return raid.state;
    }

    // increment version which will be written on disks
    raid.version++;

    // create new version variable
    char newVersion[SECTOR_SIZE];
    memcpy(newVersion, &raid.version, sizeof(unsigned int));

    // iterate over disks
    for (int i = 0; i < raid.disksCount; i++)
    {
        // if it is broken then ignore it
        if (raid.brokenDiskNumber == i) continue;

        // write new version into last sector - where we store version number
        raid.write(i, raid.sectors - 1, newVersion, 1);
    }

    // successfully change state to STOPPED
    raid.state = RAID_STOPPED;
    return raid.state;
}

int CRaidVolume::Resync() {
    // if the state is OK we do not need to resync, if we are STOPPED we do not need any changes, but state FAILED is inrecoverable
    if(raid.state == RAID_OK || raid.state == RAID_STOPPED || raid.state == RAID_FAILED) return raid.state;

    // iterate over all sectors
    for(int i = 0; i < raid.sectors; i++)
    {
        // create write and read buffer, fill write buffer with zeroes
        char writeBuffer[SECTOR_SIZE];
        char readBuffer[SECTOR_SIZE];
        memset(writeBuffer,0,SECTOR_SIZE);

        // iterate over devices
        for(int j = 0; j < raid.disksCount; j++)
        {
            // check if device is broken
            if(j == raid.brokenDiskNumber) continue;

            // try to read into buffer - we have to use internal read to surpass wrapper's check about reading from broken disk
            if(raid._read(j, i, readBuffer, 1) != 1)
            {
                //read from another disk failed
                raid.state = RAID_FAILED;
                return 0;
            }

            // count XOR to restore data
            for(int k = 0; k < SECTOR_SIZE; k++)
                writeBuffer[k] = writeBuffer[k] ^ readBuffer[k];
        }

        // write the results into disk - we have to use internal write to surpass wrapper's check about writing to broken disk
        if(raid._write(raid.brokenDiskNumber, i, writeBuffer, 1) != 1)
        {
            raid.state = RAID_DEGRADED;
            return 0;
        }
    }

    // set correct version to the nowly repaired disk
    char correctVersion[SECTOR_SIZE];
    memcpy(correctVersion, &raid.version, sizeof(unsigned int));

    if(raid._write(raid.brokenDiskNumber, raid.sectors - 1, correctVersion, 1) != 1)
    {
        raid.state = RAID_DEGRADED;
        return raid.state;
    }

    raid.brokenDiskNumber = -1;
    raid.state = RAID_OK;
    return raid.state;
}

int CRaidVolume::Status(void) const {
    return raid.state;
}

int CRaidVolume::Size(void) const {
    return (raid.sectors - 1) * (raid.disksCount - 1);
}

bool CRaidVolume::Write(int secNr, const void *data, int secCnt) {

    // if the status is failed we cannot guarantee the result so return false
    if (raid.state == RAID_FAILED) return false;

    // make correct format to data which will be written
    const char* dataToWrite = (const char*) data;

    // initialize written sectors counter
    int writtenSectors = 0;

    // initialize working sector as first sector
    int workingSector = secNr;

    // determine end sector (if it is over bounds then write only to the last one)
    int endSector = (secNr + secCnt) > Size() ? Size() : (secNr + secCnt);

    // iterate from working sector to the end sector RAID_OK version
    for (; workingSector < endSector && raid.state == RAID_OK;) {
        // retrieve real positions
        int line, disk;
        raid.getRealPositionInRAID(workingSector, disk, line);

        // determine parity disk
        int parityDisk = line % raid.disksCount;

        // read old data - checking whether the data are accessible
        char oldData[SECTOR_SIZE];
        if (!raid.read(disk, line, oldData, 1)) break;  // disk is broken

        // try to write the data
        if (!raid.write(disk, line, dataToWrite, 1)) break; //we cannot write the data to disk

        // determine the result data new xor value
        char buffer[SECTOR_SIZE];

        // read current parity value
        if (!raid.read(parityDisk, line, buffer, 1)) break; // disk is broken so we should move to another loop

        // count new xor value which will computed based on this:
        // new parity value = oldParityValue ^ oldData ^ newData
        for (int i = 0; i < SECTOR_SIZE; i++)
            buffer[i] = buffer[i] ^ oldData[i] ^ dataToWrite[i];

        // write the parity result into parity disk
        if (!raid.write(parityDisk, line, buffer, 1)) break;  // writing to the disk has failed so we should move to another look

        // increment pointer to data where we write, increment correactly written sectors counter and move working sector
        dataToWrite += SECTOR_SIZE;
        writtenSectors++;
        workingSector++;
    }

    // iterate from working sector to the end sector RAID_DEGRADED version
    for (; workingSector < endSector && raid.state == RAID_DEGRADED;)
    {
        // retrieve real positions
        int line, disk;
        raid.getRealPositionInRAID(workingSector, disk, line);

        // determine parity disk
        int parityDisk = line % raid.disksCount;

        // check if this disk is not broken
        if (raid.brokenDiskNumber != disk) {
            // read the old data
            char oldData[SECTOR_SIZE];
            if (!raid.read(disk, line, oldData, 1)) return false; //we failed in reading the data so status is FAILED so we should return negative result

            // try to write the data
            if (!raid.write(disk, line, dataToWrite, 1)) return false; // we cannot write to the disk so the status is FAILED which means our data are lost

            // increment pointer to data where we write, increment correactly written sectors counter and move working sector
            dataToWrite += SECTOR_SIZE;
            writtenSectors++;
            workingSector++;

            if (raid.brokenDiskNumber != parityDisk) {
                // compute the correct parity value
                char buffer[SECTOR_SIZE];

                // read data from parity disk
                if (!raid.read(parityDisk, line, buffer, 1))
                    return false; // we cannot read from the disk - the status is FAILED

                // compute the new parity value
                for (int i = 0; i < SECTOR_SIZE; i++)
                    buffer[i] = buffer[i] ^ oldData[i] ^ dataToWrite[i-SECTOR_SIZE];

                // write the newly computed parity value
                if (!raid.write(parityDisk, line, buffer, 1))
                    return false; // we cannot access disk so the status is FAILED and we have lost our data

            } else {
                //the broken disk is the one where should be the parity value so we can ignore it
                continue;
            }
        } else {
            // we have broken disk and we need to update the parity check
            char newParity[SECTOR_SIZE];

            // fill it with zeroes
            memset(newParity, 0, SECTOR_SIZE);

            // create buffer
            char buffer[SECTOR_SIZE];

            // iterate over devices and calculate the correct current parity value
            for (int i = 0; i < raid.disksCount; i++) {

                // check if the disk is not the parity disk
                if (i != parityDisk && i != disk) {
                    // read the data into buffer
                    raid.read(i, line, buffer, 1);

                    // if it failed then we have reach FAILED status
                    if (raid.state == RAID_FAILED)
                        return false;

                    // otherwise we can compute the correct parity valued
                    for (int j = 0; j < SECTOR_SIZE; j++)
                        newParity[j] = newParity[j] ^ buffer[j];
                }
            }

            // then xor the current parity value with new/toWrite value
            for (int j = 0; j < SECTOR_SIZE; j++)
                newParity[j] = newParity[j] ^ dataToWrite[j];

            // write the parity into disk
            if (!raid.write(parityDisk, line, newParity, 1))
                return false;

            // increment pointer to data where we write, increment correactly written sectors counter and move working sector
            dataToWrite += SECTOR_SIZE;
            writtenSectors++;
            workingSector++;
        }
    }

    return true;
}

bool CRaidVolume::Read(int secNr, void *data, int secCnt) {
    // if RAID is failed we cannot read
    if (raid.state == RAID_FAILED) return false;

    // reformat data into char pointer
    char* resultReadData = (char*) data;

    // initialize counter for positively read sections
    int readSections = 0;

    // set working sector
    int workingSector = secNr;

    // find the last sector (if over boundaries take the last one)
    int endSector = (secNr + secCnt) > CRaidVolume::Size() ? CRaidVolume::Size() : (secNr + secCnt);

    // iterate over all sectors to the endSector - RAID_OK version
    for (; workingSector < endSector && raid.state == RAID_OK;)
    {
        // get real position in RAID
        int realLine, realColumn;
        raid.getRealPositionInRAID(workingSector, realColumn, realLine);

        // read one sector
        if (raid.read(realColumn, realLine, resultReadData, 1))
        {
            // append to the result data, move working sector and increment readSections counter
            resultReadData += SECTOR_SIZE;
            workingSector++;
            readSections++;
        } else {
            // we changed state so can leave this loop
            break;
        }
    }

    // iterate over all sectors to the end sector - RAID_DEGRADED version -> we might need to compute the result
    for (; workingSector < endSector && raid.state == RAID_DEGRADED; )
    {
        // get real position in RAID
        int realLine, realColumn;
        raid.getRealPositionInRAID(workingSector, realColumn, realLine);

        // if this is not the broken disk and read correctly
        if (raid.brokenDiskNumber != realColumn){

            // if this is not the broken disk and reading gone perfectly then apppend and continue
            if(raid.read(realColumn, realLine, resultReadData, 1)) {
                // append to the result data, move working sector and increment readSections counter
                resultReadData += SECTOR_SIZE;
                workingSector++;
                readSections++;
            } else {
                // we have moved to FAILED status of RAID so we can return negative result
                return false;
            }
        } else { // if this is the broken disk
            // set data to zeroes for one sector and then modify them to correct value (we will xor over all others)
            memset(resultReadData, 0, SECTOR_SIZE);
            // create buffer to read other lines to determine the correct value of broken disk
            char buffer[SECTOR_SIZE];

            // iterate over all devices
            for (int i = 0; i < raid.disksCount; i++){

                // ignore the broken disk - we cannot trust it
                if (i != raid.brokenDiskNumber) {
                    // read the disk
                    raid.read(i, realLine, buffer, 1);
                    // if the read has failed the status will be FAILED and our data are forever lost
                    if (raid.state == RAID_FAILED) return false;

                    // otherwise compute xor value of the missing/broken disk
                    for (int j = 0; j < SECTOR_SIZE; j++)
                        resultReadData[j] = resultReadData[j] ^ buffer[j];
                }
            }

            // append to the result data, move working sector and increment readSections counter
            resultReadData += SECTOR_SIZE;
            workingSector++;
            readSections++;
        }
    }

    // read sections were read correctly
    return readSections == secCnt;
}

#ifndef __PROGTEST__
#include "tests.cpp"

#endif /* __PROGTEST__ */