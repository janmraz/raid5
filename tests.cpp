/* SW RAID5 - basic test
 *
 * The testing of the RAID driver requires a backend (simulating the underlying disks).
 * Next, the tests of your RAID implemetnation are needed. To help you with the implementation, 
 * a sample backend is implemented in this file. It provides a quick-and-dirty
 * implementation of the underlying disks (simulated in files) and a few Raid... function calls.
 *
 * The implementation in the real testing environment is different. The sample below is a 
 * minimalistic disk backend which matches the required interface. The backend, for instance, 
 * cannot simulate a crashed disk. To test your Raid implementation, you will have to modify 
 * or extend the backend.
 *
 * Next, you will have to add some raid testing. There is a few Raid... functions called from within 
 * main(), however, the tests are incomplete. For instance, RaidResync () is not tested here. Once 
 * again, this is only a starting point.
 */

long my_memcmp( char * Ptr1, char *Ptr2, size_t Count)
{
    long pos = 0;
    int i = 0;

    while (Count >= 0)
    {
        int v = Ptr1[i] - Ptr2[i];
        if (v == 0)
            pos++;
        else
            return pos;
        Count--;
        i++;
    }

    return 0;
}


const int RAID_DEVICES = 4;
const int DISK_SECTORS = 8192;
static FILE  * g_Fp[RAID_DEVICES];

//-------------------------------------------------------------------------------------------------
/** Sample sector reading function. The function will be called by your Raid driver implementation.
 * Notice, the function is not called directly. Instead, the function will be invoked indirectly
 * through function pointer in the TBlkDev structure.
 */
int                diskRead                                ( int               device,
                                                             int               sectorNr,
                                                             void            * data,
                                                             int               sectorCnt )
{
    if(device == 1)
        return 0;
    if ( device < 0 || device >= RAID_DEVICES )
        return 0;
    if ( g_Fp[device] == NULL )
        return 0;
    if ( sectorCnt <= 0 || sectorNr + sectorCnt > DISK_SECTORS )
        return 0;
    fseek ( g_Fp[device], sectorNr * SECTOR_SIZE, SEEK_SET );
    return fread ( data, SECTOR_SIZE, sectorCnt, g_Fp[device] );
}
//-------------------------------------------------------------------------------------------------
/** Sample sector writing function. Similar to diskRead
 */
int                diskWrite                               ( int               device,
                                                             int               sectorNr,
                                                             const void      * data,
                                                             int               sectorCnt )
{
    if(device == 1)
        return 0;
    if ( device < 0 || device >= RAID_DEVICES )
        return 0;
    if ( g_Fp[device] == NULL )
        return 0;
    if ( sectorCnt <= 0 || sectorNr + sectorCnt > DISK_SECTORS )
        return 0;
    fseek ( g_Fp[device], sectorNr * SECTOR_SIZE, SEEK_SET );
    return fwrite ( data, SECTOR_SIZE, sectorCnt, g_Fp[device] );
}
//-------------------------------------------------------------------------------------------------
/** A function which releases resources allocated by openDisks/createDisks
 */
void               doneDisks                               ( void )
{
    for ( int i = 0; i < RAID_DEVICES; i ++ )
        if ( g_Fp[i] )
        {
            fclose ( g_Fp[i] );
            g_Fp[i]  = NULL;
        }
}
//-------------------------------------------------------------------------------------------------
/** A function which creates the files needed for the sector reading/writing functions above.
 * This function is only needed for the particular implementation above.
 */
TBlkDev            createDisks                             ( void )
{
    char       buffer[SECTOR_SIZE];
    TBlkDev    res;
    char       fn[100];

    memset    ( buffer, 0, sizeof ( buffer ) );

    for ( int i = 0; i < RAID_DEVICES; i ++ )
    {
        snprintf ( fn, sizeof ( fn ), "/tmp/%04d", i );
        g_Fp[i] = fopen ( fn, "w+b" );
        if ( ! g_Fp[i] )
        {
            doneDisks ();
            throw "Raw storage create error";
        }

        for ( int j = 0; j < DISK_SECTORS; j ++ )
            if ( fwrite ( buffer, sizeof ( buffer ), 1, g_Fp[i] ) != 1 )
            {
                doneDisks ();
                throw "Raw storage create error";
            }
    }

    res . m_Devices = RAID_DEVICES;
    res . m_Sectors = DISK_SECTORS;
    res . m_Read    = diskRead;
    res . m_Write   = diskWrite;
    return res;
}
//-------------------------------------------------------------------------------------------------
/** A function which opens the files needed for the sector reading/writing functions above.
 * This function is only needed for the particular implementation above.
 */
TBlkDev            openDisks                               ( void )
{
    TBlkDev    res;
    char       fn[100];

    for ( int i = 0; i < RAID_DEVICES; i ++ )
    {
        snprintf ( fn, sizeof ( fn ), "/tmp/%04d", i );
        g_Fp[i] = fopen ( fn, "r+b" );
        if ( ! g_Fp[i] )
        {
            doneDisks ();
            throw "Raw storage access error";
        }
        fseek ( g_Fp[i], 0, SEEK_END );
        if ( ftell ( g_Fp[i] ) != DISK_SECTORS * SECTOR_SIZE )
        {
            doneDisks ();
            throw "Raw storage read error";
        }
    }
    res . m_Devices = RAID_DEVICES;
    res . m_Sectors = DISK_SECTORS;
    res . m_Read    = diskRead;
    res . m_Write   = diskWrite;
    return res;
}
//-------------------------------------------------------------------------------------------------
void               test1                                   ( void )
{
    /* create the disks before we use them
     */
    TBlkDev  dev = createDisks ();
    /* The disks are ready at this moment. Your RAID-related functions may be executed,
     * the disk backend is ready.
     *
     * First, try to create the RAID:
     */

    assert ( CRaidVolume::Create ( dev ) );


    /* start RAID volume */

    CRaidVolume vol;

    assert ( vol . Start ( dev ) == RAID_DEGRADED );
    assert ( vol . Status () == RAID_DEGRADED );

    /* your raid device shall be up.
     * try to read and write all RAID sectors:
     */

    cout << "ok" << endl;
    for ( int i = 0; i < vol . Size (); i ++ )
    {
        char buffer [SECTOR_SIZE];

        assert ( vol . Read ( i, buffer, 1 ) );
        assert ( vol . Write ( i, buffer, 1 ) );
    }

    /* Extensive testing of your RAID implementation ...
     */


    /* Stop the raid device ...
     */
    assert ( vol . Stop () == RAID_STOPPED );
    assert ( vol . Status () == RAID_STOPPED );

    /* ... and the underlying disks.
     */

    doneDisks ();
}
//-------------------------------------------------------------------------------------------------
void               test2                                   ( void )
{
    /* The RAID as well as disks are stopped. It corresponds i.e. to the
     * restart of a real computer.
     *
     * after the restart, we will not create the disks, nor create RAID (we do not
     * want to destroy the content). Instead, we will only open/start the devices:
     */

    TBlkDev dev = openDisks ();
    CRaidVolume vol;

    assert ( vol . Start ( dev ) == RAID_DEGRADED );


    /* some I/O: RaidRead/RaidWrite
     */

    vol . Stop ();
    doneDisks ();
}
//-------------------------------------------------------------------------------------------------
int                main                                    ( void )
{
    test1 ();
    test2 ();

    bool status = true;
    int i, retCode = 0;

    TBlkDev dev = openDisks ();
    CRaidVolume vol;

    assert ( vol . Start ( dev ) == RAID_DEGRADED );

    cout << "Test 1 (writing and reading to all sector by one sector): ";
    for ( i = 0; i < vol.Size (); i ++ )
    {
        char buffer [SECTOR_SIZE];

        retCode = vol.Write ( i, buffer, 1 );
        retCode = vol.Read ( i, buffer, 1 );
    }

    if(vol.Status() != RAID_DEGRADED)
        cout << "FAILED" << endl;
    else
        cout << "OK" << endl;

    cout << "Test 2 (writing and reading to almost all sector by many sectors): ";

    for ( i = 0; i < vol.Size (); i += 10 ){
        char buffer2 [10 * SECTOR_SIZE];
        memset(&buffer2, i, 10 * SECTOR_SIZE);

        if(!vol.Write ( i, buffer2, 10 )) {
            cout << "something wrong " << i << endl;
        }

    }


    char tempBuffer[10 * SECTOR_SIZE];
    for ( i = 0; i < vol.Size (); i += 10 ){
        char buffer3 [10 * SECTOR_SIZE];
        memset(&tempBuffer, i, 10 * SECTOR_SIZE);
        retCode = vol.Read ( i, buffer3, 10 );
        if( i + 10 < vol.Size() && memcmp(&tempBuffer, &buffer3, 10 * SECTOR_SIZE ) != 0){
            int n = my_memcmp(tempBuffer, buffer3, 10 * SECTOR_SIZE);
            status = false;
            cout << "ok - " << i << " + " << n << endl;
//            break;
        }
    }
    if(vol.Status() != RAID_DEGRADED || !status)
        cout << "FAILED" << endl;
    else
        cout << "OK" << endl;


    cout << "Test 3 (writing and reading to random sectors by various count of sectors): ";
    char buffer4 [5 * SECTOR_SIZE];
    char buffer5 [6 * SECTOR_SIZE];

    memset(&buffer4, 4, 5 * SECTOR_SIZE);
    memset(&buffer5, 5, 6 * SECTOR_SIZE);

    retCode = vol.Write ( 2000, buffer4, 5 );
    retCode += vol.Write ( 3000, buffer4, 5 );
    retCode += vol.Write ( 4000, buffer4, 5 );
    retCode += vol.Write ( 5000, buffer4, 5 );
    retCode += vol.Write ( 6000, buffer4, 5 );
    retCode += vol.Write ( 7000, buffer4, 5 );
    retCode += vol.Write ( 8000, buffer4, 5 );
    retCode += vol.Write ( 9000, buffer4, 5 );
    retCode += vol.Write ( 10000, buffer4, 5 );
    retCode += vol.Write ( 12000, buffer4, 5 );

    retCode += vol.Write ( 124, buffer5, 6 );
    retCode += vol.Write ( 5443, buffer5, 6 );
    retCode += vol.Write ( 1, buffer5, 6 );
    retCode += vol.Write ( 1432, buffer5, 6 );
    retCode += vol.Write ( 23000, buffer5, 6 );
    retCode += vol.Write ( 3333, buffer5, 6 );
    retCode += vol.Write ( 18000, buffer5, 6 );
    retCode += vol.Write ( 12876, buffer5, 6 );
    retCode += vol.Write ( 9787, buffer5, 6 );
    retCode += vol.Write ( 24565, buffer5, 6 );

    if(retCode != 110)
        cout << "return count mishmash, ";
    char tempBuffer4[5 * SECTOR_SIZE];
    char tempBuffer5[6 * SECTOR_SIZE];

    retCode =  vol.Read ( 2000, tempBuffer4, 5 );
    if(memcmp(&buffer4, &tempBuffer4, 5 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 3000, tempBuffer4, 5 );
    if(memcmp(&buffer4, &tempBuffer4, 5 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 4000, tempBuffer4, 5 );
    if(memcmp(&buffer4, &tempBuffer4, 5 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 5000, tempBuffer4, 5 );
    if(memcmp(&buffer4, &tempBuffer4, 5 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 6000, tempBuffer4, 5 );
    if(memcmp(&buffer4, &tempBuffer4, 5 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 7000, tempBuffer4, 5 );
    if(memcmp(&buffer4, &tempBuffer4, 5 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 8000, tempBuffer4, 5 );
    if(memcmp(&buffer4, &tempBuffer4, 5 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 9000, tempBuffer4, 5 );
    if(memcmp(&buffer4, &tempBuffer4, 5 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 10000, tempBuffer4, 5 );
    if(memcmp(&buffer4, &tempBuffer4, 5 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 12000, tempBuffer4, 5 );
    if(memcmp(&buffer4, &tempBuffer4, 5 * SECTOR_SIZE) != 0)
        status = false;

    retCode +=  vol.Read ( 124, tempBuffer5, 6 );
    if(memcmp(&buffer5, &tempBuffer5, 6 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 5443, tempBuffer5, 6 );
    if(memcmp(&buffer5, &tempBuffer5, 6 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 1, tempBuffer5, 6 );
    if(memcmp(&buffer5, &tempBuffer5, 6 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 1432, tempBuffer5, 6 );
    if(memcmp(&buffer5, &tempBuffer5, 6 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 23000, tempBuffer5, 6 );
    if(memcmp(&buffer5, &tempBuffer5, 6 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 3333, tempBuffer5, 6 );
    if(memcmp(&buffer5, &tempBuffer5, 6 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 18000, tempBuffer5, 6 );
    if(memcmp(&buffer5, &tempBuffer5, 6 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 12876, tempBuffer5, 6 );
    if(memcmp(&buffer5, &tempBuffer5, 6 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 9787, tempBuffer5, 6 );
    if(memcmp(&buffer5, &tempBuffer5, 6 * SECTOR_SIZE) != 0)
        status = false;
    retCode +=  vol.Read ( 24565, tempBuffer5, 6 );
    if(memcmp(&buffer5, &tempBuffer5, 6 * SECTOR_SIZE) != 0)
        status = false;
    if(retCode != 110)
        cout << "return count mishmash, ";

    if( vol.Status() != RAID_DEGRADED || !status)
        cout << "FAILED" << endl;
    else
        cout << "OK" << endl;
    return 0;
}